<?php

use Components\DB;

class TaskModel
{
    /**
     * Gets all tasks from DB
     */
    public static function getAllTasks($sort, $page)
    {
        $db = DB::getConnection();

        $quantity = 3;
        $start = ($page * $quantity) - $quantity;
        $res = $db->query("SELECT COUNT(*) FROM `tasks`");
        $row = $res->fetch();
        $totalTasks = $row[0]; // всего записей
        $totalPages = ceil($totalTasks / $quantity);

        $tasks = [];
        $result = $db->query("SELECT id, name, email, text, status FROM tasks ORDER BY {$sort} LIMIT {$start}, {$quantity}");

        if($result){
            $i = 0;
            while ($row = $result->fetch()) {
                $tasks['list'][$i]['id'] = $row['id'];
                $tasks['list'][$i]['name'] = $row['name'];
                $tasks['list'][$i]['email'] = $row['email'];
                $tasks['list'][$i]['text'] = $row['text'];
                $tasks['list'][$i]['status'] = $row['status'];
                $i++;
            }
        }
        $tasks['totalTasks'] = $totalTasks;
        $tasks['totalPages'] = $totalPages;

        return $tasks;
    }

    /**
     * Gets one task from DB
     */
    public static function getOneTasks($id)
    {
        $db = DB::getConnection();

        $result = $db->prepare("SELECT id, name, email, text, status FROM tasks WHERE `id` = :id");
        $result->execute(array('id' => $id));
        $result = $result->fetch();

        return $result;
    }

    /**
     * Chamges one task from DB
     */
    public static function changeTask($id, $status, $text)
    {
        $db = DB::getConnection();

        $sth = $db->prepare("UPDATE `tasks` SET `text` = :text, `status` = :status WHERE `id` = :id");
        $sth->execute(array('text' => $text, 'status' => $status, 'id' => $id));

        return true;
    }
}