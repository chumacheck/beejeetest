<?php
require_once('app/views/layouts/header.php');
?>

<div class="main__body flex-column">
    <main class="form-signin home-form">
        <h1 class="text-center">Добавляем задание</h1>
            <form method="POST" action="/task/create/">
                <div class="mb-3">
                    <label for="name" class="form-label">Имя</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Имя" minlength="2" required>
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" required>
                </div>
                <div class="mb-3">
                    <label for="text" class="form-label">Текст задачи</label>
                    <textarea class="form-control" id="text" rows="3" name="text" required></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Добавить задачу</button>
            </form>
        <div class="mb-3"></div>
        <a type="button" href="/" class="btn btn-primary">Вернуться к списку</a>
    </main>
</div>

<?php
require_once('app/views/layouts/footer.php');
?>