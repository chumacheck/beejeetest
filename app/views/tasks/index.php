<?php
require_once('app/views/layouts/header.php');
require_once('app/components/functions.php');
?>

        <div class="text-center main__body flex-column">
            <main class="form-signin home-form">
                <h1>Список задач</h1>
                <table id="task_list" class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col"><?= sort_link_th('Id', 'id_asc', 'id_desc'); ?></th>
                            <th scope="col"><?= sort_link_th('имя пользователя', 'name_asc', 'name_desc'); ?></th>
                            <th scope="col"><?= sort_link_th('email', 'email_asc', 'email_desc'); ?></th>
                            <th scope="col">текст задачи</th>
                            <th scope="col">статус</th>
                            <? if (AuthController::getUser() == 'admin') { ?>
                                <th scope="col">Редактирование</th>
                            <? } ?>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($tasks['list'] as $task) { ?>
                        <tr>
                            <th scope="row"><?php echo $task['id']; ?></th>
                            <td><?= $task['name']; ?></td>
                            <td><?= $task['email'] ; ?></td>
                            <td><?= $task['text']; ?></td>

                            <? if (isset($task['status']) && $task['status']) { ?>
                                <td><span class="badge badge-success">Completed</span></td>
                            <? } else {?>
                                <td><span class="badge badge-warning">Not completed</span></td>
                            <? } ?>

                            <? if (AuthController::getUser() == 'admin') { ?>
                                <td><a href="/task/edit/<?=$task['id']?>">Изменить</a></td>
                            <? } ?>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
                <div class="mb-3"></div>
                <?
                $hasFirstParam = strripos($_SERVER['REQUEST_URI'], '?') > 0;
                $hasPageParam = strripos($_SERVER['REQUEST_URI'], 'page') > 0;
                for ($i = 1; $i <= $tasks['totalPages']; $i++){?>
                    <? if ($hasFirstParam) {?>
                        <? if ($hasPageParam) {?>
                            <?$ref = substr($_SERVER['REQUEST_URI'], 0, strripos($_SERVER['REQUEST_URI'], 'page'));?>
                            <a href="<?=$ref?>page=<?=$i?>"><?=$i?></a>
                        <?} else {?>
                            <a href="<?=$_SERVER['REQUEST_URI']?>&page=<?=$i?>"><?=$i?></a>
                        <?}?>
                    <?} else {?>
                        <a href="/task/index/?page=<?=$i?>"><?=$i?></a>
                    <?}?>
                <?}
                ?>
                <div class="mb-3"></div>
                <a type="button" href="/task/add/" class="btn btn-primary">Добавить задачу</a>
            </main>
        </div>

<?php
require_once('app/views/layouts/footer.php');
?>
