<?php
require_once('app/views/layouts/header.php');
?>

<main class="form-signin">
    <form action="/auth/login/" method="post" class="registration__form">
        <h1 class="h3 mb-3 fw-normal">Вводи, если админ</h1>

        <div class="form-floating">
            <label for="login">Email address</label>
            <input type="text" class="form-control" id="login" name="login" placeholder="Admin">
        </div>
        <div class="form-floating">
            <label for="password">Password</label>
            <input type="password" class="form-control " id="password" name="password" placeholder="Password">
        </div>

        <button class="w-100 btn btn-lg btn-primary" type="submit">Login</button>
    </form>
</main>

<?php
require_once('app/views/layouts/footer.php');
?>
