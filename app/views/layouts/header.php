<?session_start();?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Igor Olkhin">
    <meta name="theme-color" content="#7952b3">
    <title>BeeJee</title>
    <link href="/app/template/css/bootstrap.min.css" rel="stylesheet">
    <link href="/app/template/css/main.css" rel="stylesheet">

</head>
<body >
<div class="header">
    <div class="header__inner">
        <ul class="nav justify-content-end custom__nav">
            <li class="nav-item">
                <? if (isset($_SESSION["session_username"]) && $_SESSION["session_username"] == 'admin') {?>
                    <a class="nav-link" aria-current="page" href="/auth/logout">Выход</a>
                <?} else {?>
                    <a class="nav-link" aria-current="page" href="/auth/">Вход для Админа</a>
                <?} ?>
            </li>

        </ul>
    </div>
</div>
