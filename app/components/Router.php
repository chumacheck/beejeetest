<?php

use Core\DatabaseConfiguration;

class Router
{
    /**
     * @var array
     */
    private $routes;
    /**
     * @var string
     */
    private $controllerName;
    /**
     * @var string
     */
    private $actionName;
    /**
     * @var string
     */
    private $modelName;

    /**
     * Class construct
     */
    public function __construct()
    {
        // Динамически вытаскиваем контроллер и метод из УРЛа
        $this->routes = explode('/', $_SERVER['REQUEST_URI']);
        $controllerURI = !empty($this->routes[1]) ? $this->routes[1] : 'Task';
        $actionURI = !empty($this->routes[2]) ? $this->routes[2] : 'index';
        $idURI = !empty($this->routes[3]) ? $this->routes[3] : '';

        // добавляем префиксы
        $this->controllerName = ucfirst($controllerURI) . 'Controller';
        $this->actionName = 'action' . ucfirst($actionURI);
        $this->modelName = ucfirst($controllerURI) . 'Model';

        // создаем контроллер
        $routesFile = 'app/controllers/' . $this->controllerName . '.php';
        if(file_exists($routesFile)) {
            include_once($routesFile);

            $controller = new $this->controllerName;
            $action = $this->actionName;

            if(method_exists($controller, $this->actionName)) {
                $controller->$action($idURI);
            } else {
               $this->openHomePage();
            }
        }  else {
            $this->openHomePage();
        }
    }

    /**
     * Send to homePage
     */
    function openHomePage()
    {
        header('Location: /');
    }
}