<?php

namespace Components;
use Core\DatabaseConfiguration;
use PDO;

class DB
{
    static public function getConnection()
    {
        $params = include('app/config/db_connection.php');
        try {
            $db = new PDO("mysql:{$params['host']}=localhost;dbname={$params['dbname']}", $params['user'], $params['password']);
            return $db;
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}