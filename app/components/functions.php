<?php
/* Функция вывода ссылок */
function sort_link_th($title, $a, $b) {
    $sort = @$_GET['sort'];

    if ($sort == $a) {
        return '<a class="active" href="/task/index/?sort=' . $b . '">' . $title . ' <i>▲</i></a>';
    } elseif ($sort == $b) {
        return '<a class="active" href="/task/index/?sort=' . $a . '">' . $title . ' <i>▼</i></a>';
    } else {
        return '<a href="/task/index/?sort=' . $a . '">' . $title . '</a>';
    }
}