<?php

include_once("app/models/TaskModel.php");
use Components\DB;

class TaskController
{
    /**
     * @var array
     */
    private $sortList = [
        'id_asc'   => '`id`',
        'id_desc'  => '`id` DESC',
        'name_asc'  => '`name`',
        'name_desc' => '`name` DESC',
        'email_asc'   => '`email`',
        'email_desc'  => '`email` DESC',
    ];

    /**
     * Opens a homePage
     */
    public function actionIndex()
    {
        $params = $this->getParams();
        $sortSql = $params['sort'];
        $pageSql = $params['page'];
        $tasks = TaskModel::getAllTasks($sortSql, $pageSql);

        require_once('app/views/tasks/index.php');
        return true;
    }

    /**
     * Shows form to add task
     */
    public function actionAdd()
    {
        require_once('app/views/tasks/taskAdd/index.php');
        return true;
    }

    /**
     * Adds one task to DB
     */
    public function actionCreate($request)
    {
        $name = $this->validate($_POST['name']);
        $email = $this->validate($_POST['email']);
        $text = $this->validate($_POST['text']);

        $db = DB::getConnection();
        $sth = $db->prepare("INSERT INTO tasks SET `name` = :name, `email` = :email, `text` = :text, `status` = :status");
        $sth->execute(array('name' => $name, 'email' => $email, 'text' => $text, 'status' => 0));

        header('Location: /');
        return true;
    }

    /**
     * Shows form to edit task
     */
    public function actionEdit($id)
    {
        $taskToEdit = TaskModel::getOneTasks($id);

        require_once('app/views/tasks/taskEdit/index.php');
        return __METHOD__;
    }

    /**
     * Change one task in DB
     */
    public function actionChange()
    {
        if (isset($_POST['status'])) {
            $status =  $this->validate($_POST['status']);
            $status = $status = 'on' ? 1 : 0;
        } else {
            $status = 0;
        }
        $text = $_POST['text'] ?  $this->validate($_POST['text']) : 0;
        $id = $_POST['id'] ?  $this->validate($_POST['id']) : 0;

        TaskModel::changeTask($id, $status, $text);
        header('Location: /');
        return true;
    }

    /**
     * Validates data
     */
    private function validate($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    /**
     * Provide params array for link in pagination
     */
    private function getParams()
    {
        $sort =  $this->validate(@$_GET['sort']);
        $page =  $this->validate(@$_GET['page']);
        $sort_sql = array_key_exists($sort, $this->sortList) ? $this->sortList[$sort] : reset($this->sortList);
        $page_sql = $page ? $page : 1;

        return [
            'sort' => $sort_sql,
            'page' => $page_sql
        ];
    }
}