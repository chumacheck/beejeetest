<?php

class AuthController
{
    /**
     * Opens an authPage
     */
    public function actionIndex()
    {
        require_once('app/views/auth/index.php');
        return true;
    }

    /**
     * Logs in User - was made only for Admin login
     */
    public function actionLogin()
    {
        session_start();

        if (isset($_POST["login"])) {
            if (!empty($_POST['login']) && !empty($_POST['password'])) {
                $login = htmlspecialchars($_POST['login']);
                $password = htmlspecialchars($_POST['password']);

                $adminParams = include('app/config/admin_info.php');
                if ($login == $adminParams['login'] && $password == $adminParams['password']) {
                    $_SESSION['session_username'] = $login;
                    header("Location: /");
                } else {
                    header("Location: /auth/");
                }
            } else {
                $message = "All fields are required!";
                header("Location: /auth/");
            }
        }
        return true;
    }

    /**
     * Logs out User
     */
    public function actionLogout()
    {
        session_start();
        unset($_SESSION['session_username']);
        session_destroy();
        header("location: /");
    }

    /**
     * Provide logged userName
     */
    public static function getUser()
    {
        if (isset($_SESSION["session_username"])) {
            return $_SESSION["session_username"];
        } else {
            return '';
        }
    }
}