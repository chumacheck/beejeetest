<?php

$configPHP = 'app/config/main.php';

if ($configPHP['mode' == 'dev']) {
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
}

require_once('app/components/Router.php');
require_once('app/components/DB.php');
require_once('app/controllers/AuthController.php');

$router = new Router();
